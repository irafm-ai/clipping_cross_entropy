# clipping_cross_entropy

The proposed version of cross-entropy utilizes adaptive clipping of loss. The data control the strength of clipping. It decreases the impact of misleading labels and helps the network reach higher precision and smaller overfit in image classification