def clipping_bce_max(y_true, y_pred):
    bce           = tf.keras.losses.BinaryCrossentropy(reduction='none')
    cross_entropy = bce(y_true, y_pred)
    cross_entropy = K.clip(cross_entropy, 0, 0.9*K.max(cross_entropy))
   
    return K.mean(cross_entropy)


def clipping_bce_mean(y_true, y_pred):
    bce           = tf.keras.losses.BinaryCrossentropy(reduction='none')
    cross_entropy = bce(y_true, y_pred)
    cross_entropy = K.clip(cross_entropy, 0, K.mean(cross_entropy))

    return K.mean(cross_entropy)
